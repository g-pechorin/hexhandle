

// 25/32/40 covered (probabl 50 as well)

// 60, 65, 80

// 3mm high

function gw_base_height() = 3.4;

module gw_base(d)
{
	cylinder(d1 = d, d2 = (d-3), h=gw_base_height());
}

module gw_big_stack(

sizes = [ // 25, 32, 40,
 50, // 60,
  65//, 80
	]
)
{
	for (i = [0:(len(sizes)-1)])
	{
		sz = sizes[i];
		translate([0, 0, i * gw_base_height()])
			gw_base(sz);
	}
	d = sizes[(len(sizes)-1)];

		translate([0, 0, (len(sizes)-1) * gw_base_height()])
	cylinder(d = (d-4), h=30);
}


module box(w,h,d)
{
	translate([w/2,0,d/2])
	cube([w,h,d],center = true);

}

module crown(cap_down = 0)
{
	intersection()
	{
		union()
		{
			box(35, 70, 10);
			scale([1, 1, -1])
				box(35, 70, 10);
		}
		translate([cap_down, 0, 0])
		union()
		{
			
			difference()
			{
				cylinder(d = 70, h=9);
				gw_big_stack();
			}
			scale([1, 1, -1])
				cylinder(d = 70, h=4);
		}
	}
}

crown();
