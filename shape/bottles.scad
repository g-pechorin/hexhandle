

use <citadel.scad>
use <vallejo.scad>

module bottles(vj = true)
{
	// sink everything 1.5cm into the thing
	sink = -15;
	
	// drop the vj pot 2cm lower than the citadel one
	vallejo = -20;
	
	sizes = [12, 18, 24];

	translate([0, 0, sink])
	{
		for (ml = sizes)//, 18, 24])
		{
			cc = 
				ml == 12 ? "red" :
				ml == 18 ? "green" :
				ml == 24 ? "blue" :
				"---";

			color(
				cc
			) gw_pot(ml);
		}

		if (vj)
			translate([0, 0, vallejo])
				color("violet") vj_pot();
	}
}

bottles();
