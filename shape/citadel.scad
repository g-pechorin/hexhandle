
// model of GW/Citadel's pot design
// intended to be used for CSG-subtract in CAD (so real pots are a bit smaller)
//
// assumes;
//	- non-tapered pots
//	- units in MM


// sauce;
//	- https://www.reddit.com/r/minipainting/comments/38tju8/paint_bottle_dimensions/

gw_pot_diameter = 
		// actually 32.5 ... but ... i want some "play"
		// 33 was "snug"
		34;

module gw_pot(ml = 12)
{
	gw_pot_height =
		12 == ml ? 45 : // 45 mm high
		18 == ml ? 50 : // 45 mm high
		24 == ml ? 60 : // 45 mm high
		null;


	gw_pot_cap_lip = 2; // how far the cap sticks out on the front
	gw_pot_cap_tail = 6; // how far the hinge sticks out back
	gw_pot_cap_peak = 3; // how far from the "top" the cap stops (cosmetic, but, use to place the cap)
	gw_pot_cap_height = 15; // how high the cap is
	gw_pot_cap_diameter = 25; // DO NOT RELY ON THIS


	// main body
	cylinder($fn = 40,d = gw_pot_diameter, h = gw_pot_height);

	// sticky off bits from the head/top
	translate([
			-(gw_pot_cap_diameter / 2),
			(gw_pot_cap_tail - gw_pot_cap_lip) / 2 + (-(gw_pot_diameter + gw_pot_cap_lip + gw_pot_cap_tail) / 2),
			gw_pot_height - gw_pot_cap_peak - gw_pot_cap_height
		]) {
		cube([
			gw_pot_cap_diameter,
			gw_pot_diameter + gw_pot_cap_lip + gw_pot_cap_tail,
			gw_pot_cap_height
		]);
	}
}

// test it
for (j = [1 : 3])
{
	translate([-(50 * j), 0, 0])
	{
		gw_pot(6 + (6 * j));
	}
}