

use <shape/bottles.scad>


// fast segment
	rows = 4; // how many rows of pots there are
	cols = 4; // how many columns of posts there are
	row_cut = 0;
	col_cut = 0;
	side = false;
	weird = false;
	link = 0;

/*
fast segment
	rows = 4; // how many rows of pots there are
	cols = 2; // how many columns of posts there are
	row_cut = 0;
	col_cut = 0;
	side = false;
*/


/*
test segment
	rows = 2; // how many rows of pots there are
	cols = 1; // how many columns of posts there are
	row_cut = 0;
	col_cut = 0;
	side = false;
	weird = false;

tooth segment
	rows = 4; // how many rows of pots there are
	cols = 3; // how many columns of posts there are
	row_cut = 2;
	col_cut = 2;
	side = false;
	weird = true;



side segment
	rows = 4; // how many rows of pots there are
	cols = 4; // how many columns of posts there are
	row_cut = 0;
	col_cut = 0;
	side = true;
	weird = false;



// linked segment
	rows = 4; // how many rows of pots there are
	cols = 4; // how many columns of posts there are
	row_cut = 2;
	col_cut = 1;
	side = false;
	weird = false;
	link = 2;

*/

start = 0; // skip this many "low" rows
flip = false;

post_diameter =
	// 42? print with ticker walls

	// ... so I went to 40
	//40;

	// ... before 45
	45;

raise = 20;

base = 20;

window_width = 25;
window_height = raise / 2;
window_length = 30;

flip_t = flip ? 0.0 : 0.5;
flip_f = 0.5 - flip_t;
post_radius = post_diameter / 2;

bump_x = cos(30) * post_diameter;
bump_y = post_diameter;

fudge = 1;



module post(vj, post_height)
{
	translate([0, 0, post_height / 2])
	{
		difference()
		{
			// main body thing
			rotate(side ? 0 : 90)
			{
				cylinder($fn=6,  h=post_height,  r=post_radius,  center=true);
			}

			// dig out the bottle and that "view window"
			{
				// bottle
				translate([0, 0, post_height / 2])
					bottles(vj);
				// view "window"
				translate([0, window_length / -2, (post_height / 2) - (window_height / 2)])
					cube([window_width, window_length, window_height + fudge], center = true);
			}
		}
	}
}

bx = side ? -bump_y : bump_x;
by = side ? bump_x : bump_y;


function hexa_coords(i, j) = 
	[
		bx * (side ? i * (3 / 4) : i + (j % 2 ? flip_t : flip_f)),
		by * (side ? j + (i % 2 ? flip_t : flip_f)  : j * (3 / 4)),
		0
	];

for ( i = [0:(cols - 1)] )
{
	for ( j = [start:(rows - 1 + start)] )
	{
		x = side ? i * (3 / 4) : i + (j % 2 ? flip_t : flip_f);
		
		y = side ? j + (i % 2 ? flip_t : flip_f)  : j * (3 / 4);

		h = side
			? j + ((i % 2) / 2)
			: j;
		
		not_cut = (weird && 0 == i) ||  (i >= col_cut || j >= row_cut);

		max_side = side && j == (rows - 1 + start) && 0 != i % 2;


		if (not_cut && !max_side)
			translate(hexa_coords(i,j))
			{
				post(
					j != 0,
					base + (raise * h)
				);
			}
	}
}

if (0 < link)
{
	i = cols;
	for ( j = [0: (link - 1)] )
	{
		x = side ? i * (3 / 4) : i + (j % 2 ? flip_t : flip_f);
		
		y = side ? j + (i % 2 ? flip_t : flip_f)  : j * (3 / 4);

		h = side
			? j + ((i % 2) / 2)
			: j;
		
		not_cut = (weird && 0 == i) ||  (i >= col_cut || j >= row_cut);

		max_side = side && j == (rows - 1 + start) && 0 != i % 2;


		if (not_cut && !max_side)
			translate([x * bx, y * by, 0])
			{
				post(
					j != 0,
					base + (raise * h)
				);
			}
	}
}