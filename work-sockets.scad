
use <hexagons.scad>

module pipe(i, h = 36, vj = true)
{
	// how much to vary the height (drop)
	drop = 0.05;
	// how much to vary the height (gain)
	gain = 0.25;



	// random varied height!
	j = ((rands(1 - drop,1 + drop + gain,1,i))[0]) * h;

	// igve it a little spin ...
	spin = ((rands(0,360/6,1,i))[0]);

	rotate([0, 0, spin])
	{
		union()
		{
			post(vj, j);
			rotate([0,0,180])
				post(vj, j);
		}
	}
}

module flat(h = 12, vj = false)
{
	scale([1.1, 1.1, 1])
	{
		post(vj, h);
		rotate([0,0,180])
			post(vj, h);
	}
}



step_x = 40;
step_y = 35;

scl = 0.9;



function coord(x, y) = 
	[
		((0 != (y % 2)) ? x + 0.5 : x) * step_x * scl,
		y * step_y * scl,
		0
	];

// pipe();
// translate([step_x / 2, step_y, 0])
// 	pipe();

translate(coord(0,0))
	flat();
translate(coord(-1,0))
	pipe(1);
translate(coord(0, 1))
	pipe(2);
translate(coord(1, 1))
	flat();
translate(coord(2, 2))
	pipe(3);
translate(coord(2, 0))
	pipe(4);
// translate(coord(1, 3))
// 	flat();
// translate(coord(2, 4))
// 	pipe();
translate(coord(0, 3))
	pipe(5);
translate(coord(0, 2))
	flat();
translate(coord(-1,2))
	pipe(6);
translate(coord(0, -1))
	pipe(7);





// module post(vj, post_height)
// {
// 	translate([0, 0, post_height / 2])
// 	{
// 		difference()
// 		{
// 			// main body thing
// 			rotate(side ? 0 : 90)
// 			{
// 				cylinder($fn=6,  h=post_height,  r=post_radius,  center=true);
// 			}

// 			// dig out the bottle and that "view window"
// 			{
// 				// bottle
// 				translate([0, 0, post_height / 2])
// 					bottles(vj);
// 				// view "window"
// 				translate([0, window_length / -2, (post_height / 2) - (window_height / 2)])
// 					cube([window_width, window_length, window_height + fudge], center = true);
// 			}
// 		}
// 	}
// }