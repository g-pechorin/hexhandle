
// Mark V (15g / 5.00m / 1h44)
handle_height = 80;

handle_foot_diameter = 25;
handle_foot_height = 20;

handle_ankle_height = 5;

handle_body_diameter = 33;

handle_neck_height = 45;
handle_neck_diameter = 15;

screw_height = 40;
screw_diameter = 5;
screw_neck = 5;

// // Mark IV (15g / 5.13m / 1h46)
// handle_height = 80;

// handle_foot_diameter = 25;
// handle_foot_height = 20;

// handle_ankle_height = 5;

// handle_body_diameter = 33;

// handle_neck_height = 40;
// handle_neck_diameter = 15;

// screw_height = 40;
// screw_diameter = 4.75;
// screw_neck = 5;

// // // // tight params
// // // handle_height = 70;
// // // handle_body_diameter = 32;
// // // handle_neck_height = 30;
// // // handle_ankle_height = 10;

// // // mark 3
// // handle_height = 80;
// // handle_body_diameter = 33;
// // handle_neck_height = 35;
// // handle_ankle_height = 5;
// // handle_foot_diameter = 25;
// // handle_foot_height = 20;
// // handle_neck_diameter = 15;
// // screw_height = 40;
// // screw_diameter = 4.5;
// // screw_neck = 5;

difference()
{
	translate([0, 0, 0])
	{
		// foot
		cylinder(
			h = handle_foot_height,
			d = handle_foot_diameter
		);

		// ankle
		translate([0, 0, handle_foot_height])
		cylinder(
			h = handle_ankle_height,
			d1 = handle_foot_diameter,
			d2 = handle_body_diameter
		);

		// body
		translate([0, 0, handle_foot_height + handle_ankle_height])
		cylinder(
			h = handle_height - (handle_neck_height + handle_ankle_height + handle_foot_height),
			d = handle_body_diameter
		);

		// neck
		translate([0, 0, handle_height - handle_neck_height])
		cylinder(
			h = handle_neck_height,
			d1 = handle_body_diameter,
			d2 = handle_neck_diameter
		);
	}
	translate([0, 0, handle_height - screw_height])
	{
		cylinder(
			h = screw_height + screw_neck,
			d = screw_diameter
		);
	};
}